/**
 * @brief MAVROS class
 * @file mavros.cpp
 * @author Vladimir Ermakov <vooon341@gmail.com>
 */
/*
 * Copyright 2013,2014,2015,2016 Vladimir Ermakov.
 *
 * This file is part of the mavros package and subject to the license terms
 * in the top-level LICENSE file of the mavros repository.
 * https://github.com/mavlink/mavros/tree/master/LICENSE.md
 */

#include <ros/console.h>
#include <mavros/mavros.h>
#include <mavros/utils.h>
#include <fnmatch.h>
#include <algorithm>

// MAVLINK_VERSION string
#include <mavlink/config.h>
#include <mavconn/mavlink_dialect.h>


using namespace mavros;

using mavlink::mavlink_message_t;
using mavconn::MAVConnInterface;
using mavconn::Framing;
using plugin::PluginBase;
using plugin::GCSPluginBase;
using utils::enum_value;


static bool is_msg_blacklisted(const mavlink_message_t *msg, const std::vector<int> &blacklist) { 
  if (!msg) {
    //ROS_INFO("msg is null");
    return false;
  }
  
  bool res = false;
  for (auto& id : blacklist) {
    if (id == msg->msgid) {
      //ROS_INFO("msg blacklisted");
      return true;
    }
  }
 
  return false;
}


MavRos::MavRos() :
	mavlink_nh("mavlink"),		// allow to namespace it
	fcu_link_diag("FCU connection"),
	gcs_link_diag("GCS bridge"),
	plugin_loader("mavros", "mavros::plugin::PluginBase"),
	gcs_plugin_loader("mavros", "mavros::plugin::GCSPluginBase"),
	last_message_received_from_gcs(0),
	plugin_subscriptions{}
{
	std::string fcu_url, gcs_url;
	std::string fcu_protocol;
	int system_id, component_id;
	int tgt_system_id, tgt_component_id;
	bool px4_usb_quirk;
	double conn_timeout_d;
	ros::V_string plugin_blacklist{}, plugin_whitelist{};
	ros::V_string gcs_plugin_blacklist{}, gcs_plugin_whitelist{};
	std::vector<int> msg_blacklist{}, msg_whitelist{};
	MAVConnInterface::Ptr fcu_link;

	ros::NodeHandle nh("~");

	nh.param<std::string>("fcu_url", fcu_url, "serial:///dev/ttyACM0");
	nh.param<std::string>("gcs_url", gcs_url, "udp://@");
	nh.param<bool>("gcs_quiet_mode", gcs_quiet_mode, false);
	nh.param("conn/timeout", conn_timeout_d, 30.0);

	nh.param<std::string>("fcu_protocol", fcu_protocol, "v2.0");
	nh.param("system_id", system_id, 1);
	nh.param<int>("component_id", component_id, mavconn::MAV_COMP_ID_UDP_BRIDGE);
	nh.param("target_system_id", tgt_system_id, 1);
	nh.param("target_component_id", tgt_component_id, 1);
	nh.param("startup_px4_usb_quirk", px4_usb_quirk, false);
	nh.getParam("plugin_blacklist", plugin_blacklist);
	nh.getParam("plugin_whitelist", plugin_whitelist);
  nh.getParam("gcs_plugin_blacklist", gcs_plugin_blacklist);
	nh.getParam("gcs_plugin_whitelist", gcs_plugin_whitelist);
	nh.getParam("msg_blacklist", msg_blacklist);
  
  ROS_INFO("Blacklisting messages: ");
  for (auto& m : msg_blacklist) {
    ROS_INFO(" - %d", m);
  }

	conn_timeout = ros::Duration(conn_timeout_d);

	// Now we use FCU URL as a hardware Id
	UAS_DIAG(&mav_uas).setHardwareID(fcu_url);

	ROS_INFO_STREAM("FCU URL: " << fcu_url);
	try {
		fcu_link = MAVConnInterface::open_url(fcu_url, system_id, component_id);
		// may be overridden by URL
		system_id = fcu_link->get_system_id();
		component_id = fcu_link->get_component_id();

		fcu_link_diag.set_mavconn(fcu_link);
		UAS_DIAG(&mav_uas).add(fcu_link_diag);
	}
	catch (mavconn::DeviceError &ex) {
		ROS_FATAL("FCU: %s", ex.what());
		ros::shutdown();
		return;
	}

	if (fcu_protocol == "v1.0") {
		fcu_link->set_protocol_version(mavconn::Protocol::V10);
	}
	else if (fcu_protocol == "v2.0") {
		fcu_link->set_protocol_version(mavconn::Protocol::V20);
	}
	//else if (fcu_protocol == "auto") {	// XXX TODO
	//	fcu_link->set_protocol_version(mavconn::Protocol::V20);
	//}
	else {
		ROS_WARN("Unknown FCU protocol: \"%s\", should be: \"v1.0\" or \"v2.0\". Used default v1.0.", fcu_protocol.c_str());
		fcu_link->set_protocol_version(mavconn::Protocol::V10);
	}

	if (gcs_url != "") {
		ROS_INFO_STREAM("GCS URL: " << gcs_url);
		try {
			gcs_link = MAVConnInterface::open_url(gcs_url, system_id, component_id);

			gcs_link_diag.set_mavconn(gcs_link);
			gcs_diag_updater.setHardwareID(gcs_url);
			gcs_diag_updater.add(gcs_link_diag);
		}
		catch (mavconn::DeviceError &ex) {
			ROS_FATAL("GCS: %s", ex.what());
			ros::shutdown();
			return;
		}
	}
	else
		ROS_INFO("GCS bridge disabled");

	// ROS mavlink bridge
	mavlink_pub = mavlink_nh.advertise<mavros_msgs::Mavlink>("from", 100);
	mavlink_sub = mavlink_nh.subscribe("to", 100, &MavRos::mavlink_sub_cb, this,
		ros::TransportHints()
			.unreliable().maxDatagramSize(1024)
			.reliable());
      
  mavlink_pub_gcs = mavlink_nh.advertise<mavros_msgs::Mavlink>("from_gcs", 100);
  mavlink_sub_gcs = mavlink_nh.subscribe("to_gcs", 100, &MavRos::mavlink_sub_cb_gcs, this,
		ros::TransportHints()
			.unreliable().maxDatagramSize(1024)
			.reliable());

	// setup UAS and diag
	mav_uas.set_tgt(tgt_system_id, tgt_component_id);
	UAS_FCU(&mav_uas) = fcu_link;

	mav_uas.add_connection_change_handler(std::bind(&MavlinkDiag::set_connection_status, &fcu_link_diag, std::placeholders::_1));
	mav_uas.add_connection_change_handler(std::bind(&MavRos::log_connect_change, this, std::placeholders::_1));
  
  // setup GCS uas
  //mav_uas.set_tgt(tgt_system_id, tgt_component_id);
	UAS_FCU(&gcs_uas) = gcs_link;

	/* load FCU plugins */
  ROS_INFO("Loading FCU plugins...");
	// issue #257 2: assume that all plugins blacklisted
	if (plugin_blacklist.empty() and !plugin_whitelist.empty())
		plugin_blacklist.emplace_back("*");

  ROS_INFO("FCU plugins: %d", gcs_plugin_loader.getDeclaredClasses().size());
	for (auto &name : plugin_loader.getDeclaredClasses()) {
    //ROS_INFO("plugin %s", name);
		add_plugin(name, plugin_blacklist, plugin_whitelist);
  }
    
  /* load GCS PLUGINS */
  ROS_INFO("Loading GCS plugins...");
  if (gcs_plugin_blacklist.empty() and !gcs_plugin_whitelist.empty())
		gcs_plugin_blacklist.emplace_back("*");

  ROS_INFO("GCS plugins: %d", gcs_plugin_loader.getDeclaredClasses().size());
	for (auto &name : gcs_plugin_loader.getDeclaredClasses()) {
    //ROS_INFO("plugin %s", name);
		add_plugin_gcs(name, gcs_plugin_blacklist, gcs_plugin_whitelist);
  }

	// connect FCU link

	// XXX TODO: move workers to ROS Spinner, let mavconn threads to do only IO
	fcu_link->message_received_cb = [this](const mavlink_message_t *msg, const Framing framing) {
		mavlink_pub_cb(msg, framing);
		plugin_route_cb(msg, framing);

		if (gcs_link) {
			if (this->gcs_quiet_mode && msg->msgid != mavlink::common::msg::HEARTBEAT::MSG_ID &&
				(ros::Time::now() - this->last_message_received_from_gcs > this->conn_timeout)) {
				return;
			}

			//gcs_link->send_message_ignore_drop(msg);
		}
	};

	fcu_link->port_closed_cb = []() {
		ROS_ERROR("FCU connection closed, mavros will be terminated.");
		ros::requestShutdown();
	};

	if (gcs_link) {
		// setup GCS link bridge
		gcs_link->message_received_cb = [this, fcu_link, msg_blacklist](const mavlink_message_t *msg, const Framing framing) {
			this->last_message_received_from_gcs = ros::Time::now();
      mavlink_pub_cb_gcs(msg, framing);
      gcs_plugin_route_cb(msg, framing);
      
      /* if message is blacklisted, do not forward it to fcu */
      if (!is_msg_blacklisted(msg, msg_blacklist)) {
        //fcu_link->send_message_ignore_drop(msg);
      } else {
        ROS_INFO("Message with id %d filtered!", msg->msgid);
      }
      
      // send the message back to gcs (test)
      //gcs_link->send_message_ignore_drop(msg);
		};

		gcs_link_diag.set_connection_status(true);
	}

	if (px4_usb_quirk)
		startup_px4_usb_quirk();

	std::stringstream ss;
	for (auto &s : mavconn::MAVConnInterface::get_known_dialects())
		ss << " " << s;

	ROS_INFO("Built-in SIMD instructions: %s", Eigen::SimdInstructionSetsInUse());
	ROS_INFO("Built-in MAVLink package version: %s", MAVLINK_VERSION);
	ROS_INFO("Known MAVLink dialects:%s", ss.str().c_str());
	ROS_INFO("MAVROS started. MY ID %u.%u, TARGET ID %u.%u",
		system_id, component_id,
		tgt_system_id, tgt_component_id);
}

void MavRos::spin()
{
	ros::AsyncSpinner spinner(4 /* threads */);

	auto diag_timer = mavlink_nh.createTimer(
			ros::Duration(0.5),
			[&](const ros::TimerEvent &) {
				UAS_DIAG(&mav_uas).update();

				if (gcs_link)
					gcs_diag_updater.update();
			});
	diag_timer.start();

	spinner.start();
	ros::waitForShutdown();

	ROS_INFO("Stopping mavros...");
	spinner.stop();
}

void MavRos::mavlink_pub_cb(const mavlink_message_t *mmsg, Framing framing)
{
	auto rmsg = boost::make_shared<mavros_msgs::Mavlink>();

	if  (mavlink_pub.getNumSubscribers() == 0)
		return;

	rmsg->header.stamp = ros::Time::now();
	mavros_msgs::mavlink::convert(*mmsg, *rmsg, enum_value(framing));
	mavlink_pub.publish(rmsg);
}


void MavRos::mavlink_pub_cb_gcs(const mavlink_message_t *mmsg, Framing framing)
{
	auto rmsg = boost::make_shared<mavros_msgs::Mavlink>();

	if  (mavlink_pub_gcs.getNumSubscribers() == 0)
		return;

	rmsg->header.stamp = ros::Time::now();
	mavros_msgs::mavlink::convert(*mmsg, *rmsg, enum_value(framing));
	mavlink_pub_gcs.publish(rmsg);
}


void MavRos::mavlink_sub_cb(const mavros_msgs::Mavlink::ConstPtr &rmsg)
{
	mavlink_message_t mmsg;

	if (mavros_msgs::mavlink::convert(*rmsg, mmsg))
		UAS_FCU(&mav_uas)->send_message_ignore_drop(&mmsg);
	else
		ROS_ERROR("Drop mavlink packet: convert error.");
}


void MavRos::mavlink_sub_cb_gcs(const mavros_msgs::Mavlink::ConstPtr &rmsg)
{
	mavlink_message_t mmsg;

	if (mavros_msgs::mavlink::convert(*rmsg, mmsg))
		UAS_FCU(&gcs_uas)->send_message_ignore_drop(&mmsg);
	else
		ROS_ERROR("Drop mavlink packet: convert error.");
}


void MavRos::plugin_route_cb(const mavlink_message_t *mmsg, const Framing framing)
{
	auto it = plugin_subscriptions.find(mmsg->msgid);
	if (it == plugin_subscriptions.end())
		return;

	for (auto &info : it->second)
		std::get<3>(info)(mmsg, framing);
}


void MavRos::gcs_plugin_route_cb(const mavlink_message_t *mmsg, const Framing framing)
{
	auto it = plugin_subscriptions_gcs.find(mmsg->msgid);
	if (it == plugin_subscriptions_gcs.end())
		return;

	for (auto &info : it->second)
		std::get<3>(info)(mmsg, framing);
}


static bool pattern_match(std::string &pattern, std::string &pl_name)
{
	int cmp = fnmatch(pattern.c_str(), pl_name.c_str(), FNM_CASEFOLD);
	if (cmp == 0)
		return true;
	else if (cmp != FNM_NOMATCH) {
		// never see that, i think that it is fatal error.
		ROS_FATAL("Plugin list check error! fnmatch('%s', '%s', FNM_CASEFOLD) -> %d",
				pattern.c_str(), pl_name.c_str(), cmp);
		ros::shutdown();
	}

	return false;
}

/**
 * @brief Checks that plugin blacklisted
 *
 * Operation algo:
 *
 *  1. if blacklist and whitelist is empty: load all
 *  2. if blacklist is empty and whitelist non empty: assume blacklist is ["*"]
 *  3. if blacklist non empty: usual blacklist behavior
 *  4. if whitelist non empty: override blacklist
 *
 * @note Issue #257.
 */
static bool is_blacklisted(std::string &pl_name, ros::V_string &blacklist, ros::V_string &whitelist)
{
	for (auto &bl_pattern : blacklist) {
		if (pattern_match(bl_pattern, pl_name)) {
			for (auto &wl_pattern : whitelist) {
				if (pattern_match(wl_pattern, pl_name))
					return false;
			}

			return true;
		}
	}

	return false;
}

inline bool is_mavlink_message_t(const size_t rt)
{
	static const auto h = typeid(mavlink_message_t).hash_code();
	return h == rt;
}

/**
 * @brief Loads plugin (if not blacklisted)
 */
void MavRos::add_plugin(std::string &pl_name, ros::V_string &blacklist, ros::V_string &whitelist)
{
	if (is_blacklisted(pl_name, blacklist, whitelist)) {
		ROS_INFO_STREAM("Plugin " << pl_name << " blacklisted");
		return;
	}

	try {
		auto plugin = plugin_loader.createInstance(pl_name);

		ROS_INFO_STREAM("Plugin " << pl_name << " loaded");

		for (auto &info : plugin->get_subscriptions()) {
			auto msgid = std::get<0>(info);
			auto msgname = std::get<1>(info);
			auto type_hash_ = std::get<2>(info);

			std::string log_msgname;

			if (is_mavlink_message_t(type_hash_))
				log_msgname = utils::format("MSG-ID (%u) <%zu>", msgid, type_hash_);
			else
				log_msgname = utils::format("%s (%u) <%zu>", msgname, msgid, type_hash_);

			ROS_DEBUG_STREAM("Route " << log_msgname << " to " << pl_name);

			auto it = plugin_subscriptions.find(msgid);
			if (it == plugin_subscriptions.end()) {
				// new entry

				ROS_DEBUG_STREAM(log_msgname << " - new element");
				plugin_subscriptions[msgid] = PluginBase::Subscriptions{{info}};
			}
			else {
				// existing: check handler message type

				bool append_allowed = is_mavlink_message_t(type_hash_);
				if (!append_allowed) {
					append_allowed = true;
					for (auto &e : it->second) {
						auto t2 = std::get<2>(e);
						if (!is_mavlink_message_t(t2) && t2 != type_hash_) {
							ROS_ERROR_STREAM(log_msgname << " routed to different message type (hash: " << t2 << ")");
							append_allowed = false;
						}
					}
				}

				if (append_allowed) {
					ROS_DEBUG_STREAM(log_msgname << " - emplace");
					it->second.emplace_back(info);
				}
				else
					ROS_ERROR_STREAM(log_msgname << " handler dropped because this ID are used for another message type");
			}
		}

		plugin->initialize(mav_uas);
		loaded_plugins.push_back(plugin);

		ROS_INFO_STREAM("Plugin " << pl_name << " initialized");
	} catch (pluginlib::PluginlibException &ex) {
		ROS_ERROR_STREAM("Plugin " << pl_name << " load exception: " << ex.what());
	}
}


/**
 * @brief Loads plugin (if not blacklisted)
 */
void MavRos::add_plugin_gcs(std::string &pl_name, ros::V_string &blacklist, ros::V_string &whitelist)
{
	if (is_blacklisted(pl_name, blacklist, whitelist)) {
		ROS_INFO_STREAM("GCS Plugin " << pl_name << " blacklisted");
		return;
	}

	try {
		auto plugin = gcs_plugin_loader.createInstance(pl_name);

		ROS_INFO_STREAM("GCS Plugin " << pl_name << " loaded");

		for (auto &info : plugin->get_subscriptions()) {
			auto msgid = std::get<0>(info);
			auto msgname = std::get<1>(info);
			auto type_hash_ = std::get<2>(info);

			std::string log_msgname;

			if (is_mavlink_message_t(type_hash_))
				log_msgname = utils::format("MSG-ID (%u) <%zu>", msgid, type_hash_);
			else
				log_msgname = utils::format("%s (%u) <%zu>", msgname, msgid, type_hash_);

			ROS_DEBUG_STREAM("Route " << log_msgname << " to " << pl_name);

			auto it = plugin_subscriptions_gcs.find(msgid);
			if (it == plugin_subscriptions_gcs.end()) {
				// new entry

				ROS_DEBUG_STREAM(log_msgname << " - new element");
				plugin_subscriptions_gcs[msgid] = GCSPluginBase::Subscriptions{{info}};
			}
			else {
				// existing: check handler message type

				bool append_allowed = is_mavlink_message_t(type_hash_);
				if (!append_allowed) {
					append_allowed = true;
					for (auto &e : it->second) {
						auto t2 = std::get<2>(e);
						if (!is_mavlink_message_t(t2) && t2 != type_hash_) {
							ROS_ERROR_STREAM(log_msgname << " routed to different message type (hash: " << t2 << ")");
							append_allowed = false;
						}
					}
				}

				if (append_allowed) {
					ROS_DEBUG_STREAM(log_msgname << " - emplace");
					it->second.emplace_back(info);
				}
				else
					ROS_ERROR_STREAM(log_msgname << " handler dropped because this ID are used for another message type");
			}
		}

		plugin->initialize(gcs_uas);
		loaded_plugins_gcs.push_back(plugin);

		ROS_INFO_STREAM("GCS Plugin " << pl_name << " initialized");
	} catch (pluginlib::PluginlibException &ex) {
		ROS_ERROR_STREAM("GCS Plugin " << pl_name << " load exception: " << ex.what());
	}
}


void MavRos::startup_px4_usb_quirk()
{
       /* sample code from QGC */
       const uint8_t init[] = {0x0d, 0x0d, 0x0d, 0};
       const uint8_t nsh[] = "sh /etc/init.d/rc.usb\n";

       ROS_INFO("Autostarting mavlink via USB on PX4");
       UAS_FCU(&mav_uas)->send_bytes(init, 3);
       UAS_FCU(&mav_uas)->send_bytes(nsh, sizeof(nsh) - 1);
       UAS_FCU(&mav_uas)->send_bytes(init, 4); /* NOTE in original init[3] */
}

void MavRos::log_connect_change(bool connected)
{
	auto ap = utils::to_string(mav_uas.get_autopilot());

	/* note: sys_status plugin required */
	if (connected)
		ROS_INFO("CON: Got HEARTBEAT, connected. FCU: %s", ap.c_str());
	else
		ROS_WARN("CON: Lost connection, HEARTBEAT timed out.");
}
