
#include <mavros/mavros_plugin.h>
#include <eigen_conversions/eigen_msg.h>
#include <thread>
#include <array>
#include <mutex>

#include <stdio.h> // standard input / output functions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <time.h>   // time calls


namespace mavros {
namespace gcs_plugins {
/**
 * @brief RTCM plugin repeater
 */
class GpsRtcmPlugin : public plugin::GCSPluginBase {
public:
  typedef std::pair<unsigned char, std::array<uint8_t, 180>> Frame;
  
public:
	GpsRtcmPlugin() : GCSPluginBase(),
		_nh("~"),
    _rate(5.0)
	{
    _nh.param("rtcm_rate", _rate, 5.0);
    _nh.param<std::string>("rtcm_url", _serial, "/dev/serial/by-id/usb-u-blox_AG_-_www.u-blox.com_u-blox_GNSS_receiver-if00");
    ROS_INFO("Rtcm rate: %f", _rate);
    ROS_INFO("Rtcm port: %s", _serial.c_str());
    
    _received = false;
    //repeater_thread = std::thread(&GpsRtcmPlugin::repeater, this);
    _port = open_port(_serial); //"/dev/serial/by-id/usb-u-blox_AG_-_www.u-blox.com_u-blox_GNSS_receiver-if00");
    configure_port(_port);
    
    
  }

	void initialize(UAS &uas_)
	{
		GCSPluginBase::initialize(uas_);

		//global_position = _nh.advertise<mavros_msgs::BasePosition>("base_position", 10);
	}

	Subscriptions get_subscriptions() {
		return {
		       make_handler(&GpsRtcmPlugin::handle_rtcm_frame)
		};
	}

private:
	ros::NodeHandle _nh;
  
  std::thread repeater_thread;

  //pobierz bity

  int getbitu(std::array<unsigned char, 180ul> buff, int pos, int len) {
    int bits = 0;
    for (int i = pos; i < pos + len; i++) bits = (int) ((bits << 1) + ((buff[i/8] >> (int) (7 - i%8)) & 1u));
    return bits;
  }

	void handle_rtcm_frame(const mavlink::mavlink_message_t *msg, mavlink::common::msg::GPS_RTCM_DATA &frame)
	{
    auto len = frame.len;
    auto data = frame.data; // numery: 1077 1087 _1230 4072
    int id = getbitu(data, 24, 12);
    
    ROS_INFO("Received RTCM frame %d, length: %d", id, len);
    uint8_t sum;
    mavlink::mavlink_update_checksum(const_cast<mavlink::mavlink_message_t*>(msg), sum);
    ROS_INFO("Checksum: %02x %02x, %04x ", msg->ck[0], msg->ck[1], msg->checksum);

    //for (uint8_t i = 0; i < msg->len; ++i) {
    //  ROS_INFO("%08x", msg->payload64[i]);
    //}
   
    _received = true;
    
    Frame new_frame;
    new_frame.first = len;
    new_frame.second = data;
    //for (unsigned i = 0; i < 180; ++i) new_frame.second[i] = 0;
    //for (unsigned i = 0; i < len; ++i) new_frame.second[i] = data[i];
    
    _mtx.lock();
    switch (id) {
      case 1077:
        frame1077 = new_frame; sendFrame(frame1077); break;
      
      case 1087:
        frame1087 = new_frame; break;
        
      case 4072:
        frame4072 = new_frame; break;
        
      case 1230:
        frame1230 = new_frame; break;
      
      default:
        break;
    };
    _mtx.unlock();
    
	}
  
  void repeater() {
    ros::Rate rate(_rate);
    int count = 0;
    while (ros::ok()) {
      if (!_received) continue;
      
      ++count;
      
      _mtx.lock();
      sendFrame(frame1077);
      //sendFrame(frame1087);
      //sendFrame(frame4072);
      
      //if (count % 10 == 0) {
      //  sendFrame(frame1230);
      //}
      _mtx.unlock();
      
      rate.sleep();
    }
  }
  
  void sendFrame(const Frame& frame) {
    //ROS_INFO("Sending frame");
    
    char n;
    fd_set rdfs;
    struct timeval timeout;
    
    // initialise the timeout structure
    timeout.tv_sec = 10; // ten second timeout
    timeout.tv_usec = 0;
    
    ROS_INFO("Sending frame size: %d", frame.first);
    
    //Create byte array
    if (frame.first == 0) {
      //ROS_INFO("Frame size == 0");
      return;
    }
    if (frame.first >= 180) {
      //ROS_INFO("Frame size > 180");
      return;
    }
    
    unsigned char send_bytes[180]; // = new unsigned char[frame.first]; //{ 0x02, 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x23, 0x01, 0x21, 0x03};
    for (unsigned int i = 0; i < frame.first; ++i) {
      send_bytes[i] = frame.second[i];
      printf("%02x ", frame.second[i]);
    }
    printf("\n");
    
    int written = write(_port, send_bytes, frame.first);  //Send data
    ROS_INFO("Bytes written: %d", written);
    //delete[] send_bytes;
    
    // do the select
    //n = select(fd + 1, &rdfs, NULL, NULL, &timeout);
  }
  
  int open_port(const std::string& dev)
  {
    int fd; // file description for the serial port
    
    fd = open(dev.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    
    if(fd == -1) // if open is unsucessful
    {
      //perror("open_port: Unable to open /dev/ttyS0 - ");
      ROS_INFO("open_port: Unable to open port");
    }
    else
    {
      fcntl(fd, F_SETFL, 0);
      ROS_INFO("port is open.\n");
    }
    
    return(fd);
  } //open_port
  
  int configure_port(int fd)      // configure the port
  {
    struct termios port_settings;      // structure to store the port settings in

    cfsetispeed(&port_settings, B57600);    // set baud rates
    cfsetospeed(&port_settings, B57600);

    port_settings.c_cflag &= ~PARENB;    // set no parity, stop bits, data bits
    port_settings.c_cflag &= ~CSTOPB;
    port_settings.c_cflag &= ~CSIZE;
    port_settings.c_cflag |= CS8;
    
    tcsetattr(fd, TCSANOW, &port_settings);    // apply the settings to the port
    return(fd);

  }
  
  bool _received;
  Frame frame1077;
  Frame frame1087;
  Frame frame4072;
  Frame frame1230;
  int _port;
  std::string _serial;
  std::mutex _mtx;
  
  double _rate;
};
}	// namespace std_plugins
}	// namespace mavros

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(mavros::gcs_plugins::GpsRtcmPlugin, mavros::plugin::GCSPluginBase)
