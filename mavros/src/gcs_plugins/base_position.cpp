
#include <mavros/mavros_plugin.h>
#include <eigen_conversions/eigen_msg.h>

#include <mavros_msgs/BasePosition.h>


namespace mavros {
namespace gcs_plugins {
/**
 * @brief Local position plugin.
 * Publish local position to TF, PositionStamped, TwistStamped
 * and Odometry
 */
class BasePositionPlugin : public plugin::GCSPluginBase {
public:
	BasePositionPlugin() : GCSPluginBase(),
		_nh("~base_position")
	{ }

	void initialize(UAS &uas_)
	{
		GCSPluginBase::initialize(uas_);

		global_position = _nh.advertise<mavros_msgs::BasePosition>("base_position", 10);
	}

	Subscriptions get_subscriptions() {
		return {
		       make_handler(&BasePositionPlugin::handle_global_position)
		};
	}

private:
	ros::NodeHandle _nh;

	ros::Publisher global_position;

	void handle_global_position(const mavlink::mavlink_message_t *msg, mavlink::common::msg::GLOBAL_POSITION_INT &pos_ned)
	{

		auto msg_send = boost::make_shared<mavros_msgs::BasePosition>();

		msg_send->header = m_uas->synchronized_header("", pos_ned.time_boot_ms);
    msg_send->latitude = pos_ned.lat;
    msg_send->longitude = pos_ned.lon;
    msg_send->altitude =  pos_ned.alt;
    msg_send->v_x = pos_ned.vx;
    msg_send->v_y = pos_ned.vy;
    msg_send->v_z = pos_ned.vz;
    msg_send->hdg = pos_ned.hdg;

		global_position.publish(msg_send);
	}
};
}	// namespace std_plugins
}	// namespace mavros

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(mavros::gcs_plugins::BasePositionPlugin, mavros::plugin::GCSPluginBase)
