
#include <mavros/mavros_plugin.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/TransformStamped.h>

#include <nav_msgs/Odometry.h>

namespace mavros {
namespace std_plugins {

class TestGCSPlugin : public plugin::GCSPluginBase {
public:
	TestGCSPlugin() : GCSPluginBase(),
		_nh("~base_position")
	{ }

	void initialize(UAS &uas_)
	{
		GCSPluginBase::initialize(uas_);
	}

	Subscriptions get_subscriptions() {
		return {
		       make_handler(&TestGCSPlugin::handle_base_position)
		};
	}

private:
	ros::NodeHandle _nh;

	ros::Publisher base_position;
	ros::Publisher base_velocity;

	void handle_base_position(const mavlink::mavlink_message_t *msg, mavlink::common::msg::GLOBAL_POSITION_INT &pos_ned)
	{

	}
};
}	// namespace std_plugins
}	// namespace mavros

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(mavros::std_plugins::TestGCSPlugin, mavros::plugin::GCSPluginBase)
